FROM node:12-alpine AS build
ARG environ
WORKDIR /usr/src/app
COPY package.json ./
RUN npm install
COPY . .
RUN npm run build-${environ}

FROM nginx:1.19-alpine
# RUN apk update && apk upgrade
COPY --from=build /usr/src/app/dist/link-repository /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
EXPOSE 8080
LABEL maintainer="Patrick Domnick <PatrickFDomnick@gmail.com>"
ENTRYPOINT ["nginx", "-g", "daemon off;"]
