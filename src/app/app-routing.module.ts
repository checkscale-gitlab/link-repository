import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserGuard } from './user/user.guard';

import { TabelViewComponent } from './tabel-view/tabel-view.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AnonymusComponent } from './anonymus/anonymus.component';
import { TosComponent } from './tos/tos.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { UserComponent } from './user/user.component';
import { UserLoginComponent } from './user-login/user-login.component';
import { UserRegisterComponent } from './user-register/user-register.component';
import { UserConfirmComponent } from './user-confirm/user-confirm.component';
import { UserManageComponent } from './user-manage/user-manage.component';
import { UserResetComponent } from './user-reset/user-reset.component';

const routes: Routes = [
  { path: '', component: AnonymusComponent },
  { path: 'view', component: TabelViewComponent, canActivate: [UserGuard] },
  {
    path: 'user',
    component: UserComponent,
    children: [
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
      },
      {
        path: 'login',
        component: UserLoginComponent,
      },
      {
        path: 'register',
        component: UserRegisterComponent,
      },
      {
        path: 'confirm',
        component: UserConfirmComponent,
      },
      {
        path: 'manage',
        component: UserManageComponent,
      },
      {
        path: 'reset',
        component: UserResetComponent,
      },
    ],
  },
  { path: 'tos', component: TosComponent },
  { path: 'privacy', component: PrivacyComponent },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
