import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root',
})
export class UserGuard implements CanActivate {
  constructor(private userService: UserService, public router: Router) {}

  async canActivate(): Promise<boolean> {
    const auth = await this.userService.checkAuthenticated().toPromise();
    if (!auth.valueOf()) {
      this.router.navigate(['user']);
      return false;
    } else {
      return true;
    }
  }
}
