import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  tabs: any[];

  constructor(private userService: UserService) {
    this.tabs = [
      {
        title: 'Login',
        icon: 'person-done-outline',
        route: '/user/login',
        disabled: this.userService.isAuth,
      },
      {
        title: 'Register',
        icon: 'person-add-outline',
        route: '/user/register',
        disabled: this.userService.isAuth,
      },
      {
        title: 'Confirm',
        icon: 'checkmark-circle-outline',
        route: '/user/confirm',
        disabled: this.userService.isAuth,
      },
      {
        title: 'Manage',
        icon: 'email-outline',
        route: '/user/manage',
        disabled: !this.userService.isAuth,
      },
      {
        title: 'Reset',
        icon: 'repeat-outline',
        route: '/user/reset',
        disabled: this.userService.isAuth,
      }
    ];
  }

  ngOnInit(): void {
  }

}
