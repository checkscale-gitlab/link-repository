import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { UserService } from '../user/user.service';
import { NbToastrService } from '@nebular/theme';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-confirm',
  templateUrl: './user-confirm.component.html',
  styleUrls: ['./user-confirm.component.scss'],
})
export class UserConfirmComponent implements OnInit {
  formControlUsername = new FormControl('');
  formControlCode = new FormControl('');

  constructor(
    private userService: UserService,
    private router: Router,
    private toastrService: NbToastrService
  ) {}

  ngOnInit(): void {}

  confirm(): void {
    let username: string;
    let code: string;

    username = this.formControlUsername.value;
    code = this.formControlCode.value;

    const tConfirm = this.toastrService.show('Checking Code...', 'Confirm', {
      status: 'info',
    });
    const sConfirm = this.userService.confirm(username, code);
    sConfirm.subscribe(
      (x) => {
        tConfirm.close();
        this.toastrService.show('Account activated!', 'Success', {
          status: 'success',
        });
        this.router.navigate(['/user/login']);
      },
      (err: Error) => {
        tConfirm.close();
        this.toastrService.show(err.message, 'Error', {
          status: 'danger',
        });
      }
    );
  }
}
